
const express = require('express');
const {registerUser, login} = require('../controllers/authController.js');
const router = new express.Router();

// register new user
router.post('/api/auth/register', registerUser);

// login
router.post('/api/auth/login', login);

module.exports = router;
