
const express = require('express');
const auth = require('../utils/auth/auth.js');
const {
  getProfileInfo,
  changeUserPassword,
  deleteUser,
} = require('../controllers/userController.js');
const router = new express.Router();

// get user's profile info
router.get('/api/users/me', auth, getProfileInfo);

// change user's password
router.patch('/api/users/me', auth, changeUserPassword);

// delete user's profile
router.delete('/api/users/me', auth, deleteUser);

module.exports = router;
