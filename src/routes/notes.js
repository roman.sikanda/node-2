
const express = require('express');
const auth = require('../utils/auth/auth.js');
const {
  addNote,
  getNotes,
  getNoteById,
  updateNote,
  toggleNoteCompletion,
  deleteNoteById,
} = require('../controllers/noteController.js');
const router = new express.Router();

// add Note for User
router.post('/api/notes', auth, addNote);

// get user's notes
router.get('/api/notes', auth, getNotes);

// get user's note by id
router.get('/api/notes/:id', auth, getNoteById);

// update user's note by id
router.put('/api/notes/:id', auth, updateNote);

// check/uncheck user's note by id
router.patch('/api/notes/:id', auth, toggleNoteCompletion);

// delete user's note by id
router.delete('/api/notes/:id', auth, deleteNoteById);

module.exports = router;
