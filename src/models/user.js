
const mongoose = require('mongoose');

const userSchema = new mongoose.Schema(
    {
      username: {
        type: String,
        required: [true, 'Please enter the username'],
        unique: [true, `This username was used already`],
      },
    },
    {
      timestamps: {
        createdAt: 'createdDate',
        updatedAt: false,
      },
    },
);

module.exports = mongoose.model('User', userSchema);
