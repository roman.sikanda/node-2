
const mongoose = require('mongoose');

const noteSchema = new mongoose.Schema(
    {
      text: {
        type: String,
        required: [true, 'Please add a text value'],
      },
      userId: {
        type: String,
        required: [true, 'No user id provided'],
      },
      completed: {
        type: Boolean,
        default: false,
      },
    },
    {
      timestamps: {
        createdAt: 'createdDate',
        updatedAt: false,
      },
    },
);

module.exports = mongoose.model('Note', noteSchema);
