
const mongoose = require('mongoose');

const credentialsSchema = new mongoose.Schema(
    {
      username: {
        type: String,
        required: [true, 'Please add a text value'],
        unique: [true, `This username was used already`],
      },
      password: {
        type: String,
        required: [true, 'Please enter a password'],
      },
    },
    {
      timestamps: false,
    },
);

module.exports = mongoose.model('Credentials', credentialsSchema);
