const jwt = require('jsonwebtoken');
const User = require('../../models/user.js');

const auth = async (req, res, next) => {
  let token;
  if (
    req.headers.authorization &&
    (req.headers.authorization.startsWith('Bearer') ||
      req.headers.authorization.startsWith('JWT'))
  ) {
    try {
      // get token from header
      token = req.headers.authorization.split(' ')[1];

      // verify token
      const decoded = jwt.verify(token, process.env.JWT_SECRET);

      // get user from the token
      req.user = await User.findById(decoded.id).select('-__v -createdDate');
      next();
    } catch (error) {
      return res.status(400).send({message: 'Not authorized'});
    }
  }
  if (!token) {
    res.status(400).send({message: 'Not authorized, no token'});
  }
};

module.exports = auth;
