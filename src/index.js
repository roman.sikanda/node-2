const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
require('dotenv').config();

const usersRouter = require('./routes/users.js');
const notesRouter = require('./routes/notes.js');
const authRouter = require('./routes/auth.js');
const loggerMiddleware = require('./utils/logs/logger.js');

const connectDB = require('./utils/config/db.js');
connectDB();

const app = express();
const PORT = process.env.PORT || 8080;

app.use(express.json());
app.use(cors());
app.use(morgan(':method :url :status - :response-time ms'));
app.use(loggerMiddleware);

app.use(usersRouter);
app.use(notesRouter);
app.use(authRouter);

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
