
const Note = require('../models/note.js');

const addNote = async (req, res) => {
  const userId = req?.user?._id;
  if (!userId) {
    return res
        .status(400)
        .send({message: 'You are not authorized. userId not found'});
  }
  try {
    if (!req?.body?.text) {
      res.status(400).send({message: 'Please add a text field'});
    }
    await Note.create({
      text: req.body.text,
      userId,
      completed: req.body?.completed ?? false,
    });
    res.send({message: 'Success'});
  } catch (error) {
    res.status(500).send({message: 'Server error. Note was not added.'});
  }
};

const getNotes = async (req, res) => {
  const userId = req?.user?._id;
  const {offset=0, limit=5} = req.query;
  let count;
  if (!userId) {
    return res
        .status(400)
        .send({message: 'You are not authorized. userId not found'});
  }
  try {
    const notes = await Note.find({userId}).select('-__v')
        .skip(offset).limit(limit);
    count = notes?.length ?? 0;
    res.status(200).send({offset, limit, count, notes});
  } catch (error) {
    res.status(500).send({message: `Server error: ${error}`});
  }
};

const getNoteById = async (req, res) => {
  const userId = req?.user?._id;
  const noteId = req.params?.id;
  if (!userId) {
    return res
        .status(400)
        .send({message: 'You are not authorized. userId not found'});
  } else if (!noteId) {
    return res.status(400).send({message: 'Please provide noteId'});
  }
  try {
    const notes = await Note.find({userId}).select('-__v');
    const note = notes.filter((note) => note._id.toString() === noteId);
    res.send({note});
  } catch (error) {
    res.status(500).send({message: `Server error: ${error}`});
  }
};

const updateNote = async (req, res) => {
  const userId = req?.user?._id;
  const id = req.params?.id;
  if (!userId) {
    return res
        .status(400)
        .send({message: 'You are not authorized. userId not found'});
  } else if (!id) {
    res.status(400).send({message: 'Id not provided'});
  }
  try {
    const note = await Note.findById(id);
    if (!note || note.userId !== userId.toString()) {
      return res.status(400).send({message: 'Note was not found'});
    }
    note.text = req.body?.text ?? ' ';
    note.save();
    res.send({message: 'Updated successully'});
  } catch (error) {
    res.status(500).send({message: 'Note was not updated'});
  }
};

const toggleNoteCompletion = async (req, res) => {
  const userId = req?.user?._id;
  const noteId = req.params?.id;
  if (!userId) {
    return res
        .status(400)
        .send({message: 'You are not authorized. userId not found'});
  } else if (!noteId) {
    return res.status(400).send({message: 'Please provide noteId'});
  }
  try {
    const note = await Note.findById(noteId);
    if (!note || note.userId !== userId.toString()) {
      return res.status(400).send({message: 'Note was not found'});
    }
    note.completed = !note.completed;
    await note.save();
    res.send({message: 'Success'});
  } catch (error) {
    res.status(500).send({message: `Server error: ${error}`});
  }
};

const deleteNoteById = async (req, res) => {
  const userId = req?.user?._id;
  const noteId = req.params?.id;
  if (!userId) {
    return res
        .status(400)
        .send({message: 'You are not authorized. userId not found'});
  } else if (!noteId) {
    return res.status(400).send({message: 'Please provide noteId'});
  }
  try {
    const note = await Note.findOneAndDelete({_id: noteId, userId});
    if (!note) {
      return res.status(400).send({message: 'You do not have such note'});
    }
    res.send({message: 'Success'});
  } catch (error) {
    res.status(500).send({message: `Server error: ${error}`});
  }
};

module.exports = {
  addNote,
  getNotes,
  getNoteById,
  updateNote,
  toggleNoteCompletion,
  deleteNoteById,
};
