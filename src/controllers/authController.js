
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const User = require('../models/user.js');
const Credentials = require('../models/credentials.js');

const generateToken = (id) => {
  return jwt.sign({id}, process.env.JWT_SECRET, {
    expiresIn: '240h',
  });
};
const registerUser = async (req, res) => {
  const username = req.body?.username;
  const password = req.body?.password;
  if (!username) {
    return res.status(400).send({message: 'Please enter a username'});
  } else if (!password) {
    return res.status(400).send({message: 'Please enter a password'});
  }
  try {
    const hashedPassword = await bcrypt.hash(password, 10);
    const user = await User.create({
      username,
    });
    await Credentials.create({
      username,
      password: hashedPassword,
    });
    // usefully to send back auth token after successful registration
    const token = generateToken(user._id);
    res.send({
      message: 'Success',
      jwt_token: token,
    });
  } catch (error) {
    return res
        .status(500)
        .send({message: 'Server error. User wasn\'t registered', error});
  }
};
const login = async (req, res) => {
  const username = req.body?.username;
  const password = req.body?.password;
  if (!username) {
    return res.status(400).send({message: 'Please enter a username'});
  } else if (!password) {
    return res.status(400).send({message: 'Please enter a password'});
  }
  try {
    const credentials = await Credentials.findOne({username});
    if (!credentials) {
      return res.status(400).send({message: 'Invalid credentials'});
    }
    const passwordComparingResult = await bcrypt.compare(
        password,
        credentials.password,
    );
    if (!passwordComparingResult) {
      return res.status(400).send({message: 'Invalid credentials'});
    }
    // returning token after successful login
    const user = await User.find({username});
    const userId = user[0]._id;
    const token = generateToken(userId);
    res.send({message: 'Success', jwt_token: token});
  } catch (error) {
    return res.status(500).send({message: `Server error. ${error}`});
  }
};

module.exports = {registerUser, login};
