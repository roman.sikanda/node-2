
const bcrypt = require('bcrypt');
const User = require('../models/user.js');
const Credentials = require('../models/credentials.js');

const getProfileInfo = async (req, res) => {
  const userId = req?.user?._id;
  if (!userId) {
    return res
        .status(400)
        .send({message: 'You are not authorized. userId not found'});
  }
  try {
    const profileInfo = await User.findOne({_id: userId}).select('-__v');
    res.send({user: profileInfo});
  } catch (error) {
    res.status(500).send({message: `Server error: ${error}`});
  }
};

const changeUserPassword = async (req, res) => {
  const username = req?.user?.username;
  const oldPassword = req?.body?.oldPassword;
  const newPassword = req?.body?.newPassword;
  if (!username) {
    return res.status(400).send({message: 'You are not authorized.'});
  } else if (!oldPassword) {
    return res.status(400).send({message: 'Old password is required.'});
  } else if (!newPassword) {
    return res.status(400).send({message: 'New password is required.'});
  }
  try {
    const credentials = await Credentials.findOne({username});
    const isOldPasswordRight = await bcrypt.compare(
        req.body.oldPassword,
        credentials.password,
    );
    if (isOldPasswordRight) {
      credentials.password = await bcrypt.hash(newPassword, 10);
    }
    res.send({message: 'Success'});
  } catch (error) {
    res.status(500).send({message: `Server error: ${error}`});
  }
};

const deleteUser = async (req, res) => {
  // delete user from credentials and from users
  const userId = req?.user?._id;
  const username = req?.user?.username;
  if (!userId || !username) {
    return res.status(400).send({message: 'You are not authorized.'});
  }
  try {
    await User.findByIdAndDelete(userId);
    await Credentials.findOneAndDelete({username});
    res.send({message: 'Success'});
  } catch (error) {
    res.status(500).send({message: `Server error: ${error}`});
  }
};

module.exports = {getProfileInfo, changeUserPassword, deleteUser};
